from __future__ import division
import sys, pygame, pygame.midi
from multiprocessing import Process, Lock, Manager, Value
import time
import serial



buffer_size = 4096
read_size = 1024
num_threads = 3
midi_mutex = Lock()
counter_mutex = Lock()
manager = Manager()
poll_duration = 9000


thread_song_info = []
for i in range(0, num_threads):
	thread_song_info.append(manager.list())
#thread_song_info = [[]] * num_threads

global_raw_song = []
global_song = []

def poll(id, midi_input, thread_data, shared_counter):	    
	#note_status = 144
	first_note = False	
	# run the polling loop
	while shared_counter.value < poll_duration:
		#if pygame.midi.Input(id).poll():
		# no way to find number of messages in queue
		# so we just specify the max read value
		msg = []
		with midi_mutex:
			msg = midi_input.read(read_size)

		#reset loop if we see a note
		if len(msg) > 0:
			#add all raw received note data
			thread_data.append(msg)
			if first_note:
				with counter_mutex:
					shared_counter.value = 0
			else:
				first_note = True
		else:
			with counter_mutex:
                        	shared_counter.value += 1

	#print remove_empty(thread_song_info[t_id])

def remove_empty(raw_song_info):
	non_empty_list = []
	for i in range(len(raw_song_info)):
		if len(raw_song_info[i]) > 0:
			non_empty_list.append(raw_song_info[i])
	return non_empty_list

def parse_raw_notes(raw_song_info):
	current_note = {}
	for i in range(0, len(raw_song_info)):	
		time_stamp = raw_song_info[i][1]
		note_value = raw_song_info[i][0][1]
		note_released = raw_song_info[i][0][2] == 0
		if note_released:
			# either release of note just pressed (set note duration)
			# or some other previous note (ignore)
			if 'note' in current_note and note_value == current_note['note']:
				current_note['time_released'] = time_stamp
				global_song.append(current_note)
				current_note = {}
		else:
			if len(current_note) > 0:
				# we may have to terminate the previous one
				# if that note has not been released yet
				current_note['time_released'] = time_stamp
				global_song.append(current_note)
				current_note = {}
			current_note['note'] = note_value
			current_note['time_pressed'] = time_stamp
	#print song_info
	#for i in range(0, len(song_info)):
	#	print song_info[i]['note']

def flatten_note_list(raw_song_info):
	flattened = []
	for i in range(0, len(raw_song_info)):
		for j in range(0, len(raw_song_info[i])):
			flattened.append(raw_song_info[i][j])
	return flattened

def merge_song_data(raw_song_data):
	for i in range(0, len(raw_song_data)):
		time_stamp = raw_song_data[i][1]
		#check for dup time stamps
		dup = False
		for j in range (0, len(global_raw_song)):
			if time_stamp == global_raw_song[j][1]:
				dup = True
				break
		if dup == False:
			global_raw_song.append(raw_song_data[i])

def convert_to_roomba(song_dictionary):
	# Open a serial connection to Roomba
	ser = serial.Serial(port='/dev/ttyUSB0', baudrate=115200)
	# Assuming the robot is awake, start safe mode so we can hack.
	ser.write('\x83')

	num_roomba_songs = 4
	idx = 0
	song_num = 0
	song_cmd = []
	while idx < len(song_dictionary):
		#create a song up to max length at a time
		[song_dic, idx] = create_roomba_song(song_dictionary, idx, song_num)
		song_cmd.append(song_dic)
		song_num = (song_num + 1) % num_roomba_songs
	play_song_cmds = []
	sleep_durs = []
	i=0
	while i < len(song_cmd):
		end_inner = min(i + num_roomba_songs, len(song_cmd))
		for song_num in range(i, end_inner): 
			#play song
			#print str(song_cmd)
			song_data = song_cmd[song_num]
			if song_data is None:
				break
			song = song_data['song_cmd']
			song_dur_s = song_data['song_dur_s']
			ser.write(song)
			print '\\x' + '\\x'.join(x.encode('hex') for x in song)
			play_song_cmds.append('\x8d' + chr(song_num % num_roomba_songs))
			print 'sleep duration: %f' % song_dur_s
			sleep_durs.append(song_dur_s)
		eps = 0.005
		num_attempts = 16
		for k in range(i, end_inner):
			#attempt to write to play the song multiple times in case of failure
			for attempt_num in range(0, num_attempts):
				ser.write(play_song_cmds[k])
				time.sleep(eps)
			time.sleep(sleep_durs[k]) # wait for the song to complete
		i += num_roomba_songs
	time.sleep(.5) # make sure everything is finished before closing
	# Leave the Roomba in passive mode; this allows it to keep
	#  running Roomba behaviors while we wait for more commands.
	ser.write('\x80')
	# Close the serial port; we're done for now.
	ser.close()

def create_roomba_song(song_dictionary, idx, song_num):
	max_length = 16
	rest_note = 30
	note_dur_factor = 64
	min_dur = 8
	end_of_song = min(idx + max_length, len(song_dictionary))
	song_dur_s = 0.0
	song_cmd = '\x8c'
	song_cmd += chr(song_num)
	prev_release_time_ms = sys.maxint
	song_length = 0
	note_cmd = ''
	while idx < end_of_song and song_length < 16:
		note = song_dictionary[idx]
		#print note
		dur_ms = 0
		note_val = rest_note
		if note['time_pressed'] > prev_release_time_ms:
			dur_ms = note['time_pressed'] - prev_release_time_ms
			prev_release_time_ms = note['time_pressed']
		else:
			dur_ms = note['time_released'] - note['time_pressed']
			note_val = note['note']
			prev_release_time_ms = note['time_released']
			idx += 1
		dur = note_dur_factor * dur_ms // 1000
		dur_adjusted = max(min_dur, min(255, dur+1))
		song_dur_s += 1.0 * float(dur_adjusted) / float(note_dur_factor)
		print "note: %d duration: %d dur_adj: %d dur_total: %f" % (note_val, dur_ms, dur_adjusted, song_dur_s)
		note_cmd += chr(note_val)
		note_cmd += chr(dur_adjusted)
		song_length += 1
	print "length of song: %d" % song_length
	song_cmd += chr(song_length)
	song_cmd += note_cmd
	return_dic = {}
	return_dic['note_num'] = song_length
	return_dic['song_cmd'] = song_cmd
	return_dic['song_dur_s'] = song_dur_s
	return [return_dic, idx]

# set up pygame
pygame.init()
pygame.midi.init()

# list all midi devices
for x in range(0, pygame.midi.get_count()):
    print pygame.midi.get_device_info(x)

# open a specific midi device
id = pygame.midi.get_count()-1
input = pygame.midi.Input(id, buffer_size)
threads = [None] * num_threads
while True:
	shared_counter = Value('i', 0)
	for i in range(0, num_threads):
		threads[i] = Process(target=poll, args=(id,input,thread_song_info[i], shared_counter,))
	for i in range(0, num_threads):
		threads[i].start()
	for i in range(0, num_threads):
		threads[i].join()
		#print "stage 0: %s" % thread_song_info[i]
		#thread_song_info[i] = remove_empty(thread_song_info[i])
		#print "stage 1 (%d): %s" % (i, thread_song_info[i])
		thread_song_info[i] = flatten_note_list(thread_song_info[i])
		#print "stage 2 (%d): %s" % (i, thread_song_info[i])
		merge_song_data(thread_song_info[i])
		#print "stage 3 (%d): %s" % (i, global_raw_song)

		#reset thread data list
		thread_song_info[i] = manager.list()

	#print "stage 4: %s" % global_raw_song
	global_raw_song = sorted(global_raw_song, key=lambda k: k[1])
	parse_raw_notes(global_raw_song)
	global_song = sorted(global_song, key=lambda k: k['time_pressed'])
	print "stage final:"
	for i in range(0, len(global_song)):
		print global_song[i]
	convert_to_roomba(global_song)
	global_raw_song = []
	global_song = []
