import sys, pygame, pygame.midi
import threading
import time


def poll(id):	    
	sleep_time = 1
	idle_poll_duration = 8000
	read_size = 1024
	note_status = 144

	raw_song_info = []
	idle = 0
	# run the polling loop
	while idle < idle_poll_duration:
		#if pygame.midi.Input(id).poll():
		# no way to find number of messages in queue
		# so we just specify the max read value
		msg = pygame.midi.Input(id).read(read_size)

		#add all raw received note data
		if len(msg) > 0:
			print msg
			raw_song_info.append(msg)
			idle = 0
		#time.sleep(sleep_time)
		idle += sleep_time
	parse_raw_notes(raw_song_info)

def parse_raw_notes(raw_song_info):
	song_info = []
	current_note = {}
	raw_song_info = flatten_note_list(raw_song_info)
	for i in range(0, len(raw_song_info)):	
		time_stamp = raw_song_info[i][1]
		note_value = raw_song_info[i][0][1]
		note_released = raw_song_info[i][0][2] == 0
		if note_released:
			# either release of note just pressed (set note duration)
			# or some other previous note (ignore)
			if 'note' in current_note and note_value == current_note['note']:
				current_note['time_released'] = time_stamp
				song_info.append(current_note)
				current_note = {}
		else:
			if len(current_note) > 0:
				# we may have to terminate the previous one
				# if that note has not been released yet
				current_note['time_released'] = time_stamp
				song_info.append(current_note)
				current_note = {}
			current_note['note'] = note_value
			current_note['time_pressed'] = time_stamp
	print song_info
	for i in range(0, len(song_info)):
		print song_info[i]['note']

def flatten_note_list(raw_song_info):
	flattened = []
	for i in range(0, len(raw_song_info)):
		for j in range(0, len(raw_song_info[i])):
			flattened.append(raw_song_info[i][j])
	return flattened

# set up pygame
pygame.init()
pygame.midi.init()

# list all midi devices
for x in range( 0, pygame.midi.get_count() ):
    print pygame.midi.get_device_info(x)

# open a specific midi device
id = pygame.midi.get_count()-1
while True:
	t = threading.Thread(target=poll, args=(id,))
	t.start()
	t.join()
