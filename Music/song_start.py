import serial
import time

# Open a serial connection to Roomba
ser = serial.Serial(port='/dev/ttyUSB0', baudrate=115200)

# Assuming the robot is awake, start safe mode so we can hack.
ser.write('\x83')
time.sleep(.1)

# Entertain
ser.write('\x8c\x01\x06\x62\x10\x64\x10\x60\x10\x5D\x20\x5F\x10\x5B\x20')
ser.write('\x8d\x01')
time.sleep(2.1) # wait for the song to complete

ser.write('\x8c\x02\x06\x56\x10\x58\x10\x54\x10\x51\x20\x53\x10\x4F\x20')
ser.write('\x8d\x02')
time.sleep(2.1) # wait for the song to complete

ser.write('\x8c\x03\x09\x4A\x10\x4C\x10\x48\x10\x45\x20\x47\x10\x45\x10\x44\x10\x43\x40\x4F\x20')
ser.write('\x8d\x03')
time.sleep(3.6) # wait for the song to complete

ser.write('\x8c\x00\x08\x4A\x10\x4B\x10\x4C\x10\x54\x20\x4C\x10\x54\x20\x4C\x10\x54\x20')
ser.write('\x8d\x00')
time.sleep(3.0) # wait for the song to complete

ser.write('\x8c\x00\x0A\x54\x10\x56\x10\x57\x10\x58\x10\x54\x10\x56\x10\x58\x20\x53\x10\x56\x20\x54\x20')
ser.write('\x8d\x00')
time.sleep(3.3) # wait for the song to complete

# Leave the Roomba in passive mode; this allows it to keep
#  running Roomba behaviors while we wait for more commands.
ser.write('\x80')
# Close the serial port; we're done for now.
ser.close()
